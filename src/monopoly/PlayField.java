/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;

public class PlayField {
    private static final Random RND = new Random();
    private final int width, height;

    // Координаты начала и конца каждой линии.
    private final int[] lineStart;
    private final int[] lineEnd;

    // Рзамер поля (общее количество клеток в нём).
    public final int size;

    // Проверяем, сколько клеток на линии уже заполнены.
    private final int[] lineFilled = new int[]{3, 3, 3, 3};

    public Cell[] field;

    public PlayField(int height, int width) {
        this.width = width;
        this.height = height;
        this.size = 2 * height + 2 * width - 4;

        this.lineStart = new int[]{0, width - 1, width + height - 2, 2 * width + height - 3};
        this.lineEnd = new int[]{width - 1, width + height - 2, 2 * width + height - 3, 2 * width + 2 * height - 4};

        field = new Cell[2 * height + 2 * width - 4];

        // Генерация поля.
        generateField();
    }

    /**
     * Генерация клеток поля.
     */
    private void generateField() {
        // Заполняем пустые клетки.
        generateEmptyCells();

        // Заполняем отделения банка.
        generateBank();

        // Заполняем клетки-такси.
        generateTaxi();

        // Заполняем клетки-пенальти.
        generatePenalty();

        // Заполняем магазины.
        generateShops();
    }

    /**
     * Генерация пустых клеток поля.
     */
    public void generateEmptyCells() {
        for (int i = 0; i < 4; i++) {
            field[lineStart[i]] = new EmptyCell();
        }
    }

    /**
     * Генерация клеток - офисов банка.
     */
    public void generateBank() {
        for (int i = 0; i < 4; i++) {
            field[getRandomCell(lineStart[i] + 1, lineEnd[i] - 1)] = new Bank();
        }
    }

    /**
     * Генерация клеток такси.
     */
    public void generateTaxi() {
        int lineCnt, start, end;

        // Проходимся по каждой линии.
        for (int i = 0; i < 4; i++) {
            // Количество такси на линии.
            lineCnt = RND.nextInt(3);

            // Вносим количество заполненных клеток линии.
            lineFilled[i] += lineCnt;
            start = lineStart[i] + 1;
            end = lineEnd[i] - 1;

            for (int j = 0; j < lineCnt; j++) {
                int chooseCell = getRandomCell(start, end);
                while (field[chooseCell] != null) {
                    chooseCell = getRandomCell(start, end);
                }

                field[chooseCell] = new Taxi(size);
            }
        }
    }

    /**
     * Генерация штрафных клеток поля.
     */
    public void generatePenalty() {
        int lineCnt, start, end;

        // Проходимся по каждой линии.
        for (int i = 0; i < 4; i++) {
            // Количество клеток-пенальти на линии с учётом того, сколько вообще свободных клеток осталось.
            lineCnt = RND.nextInt(i % 2 == 0 ? Math.min(width - lineFilled[i] + 1, 3) :
                    Math.min(height - lineFilled[i] + 1, 3));
            start = lineStart[i] + 1;
            end = lineEnd[i] - 1;

            for (int j = 0; j < lineCnt; j++) {
                int chooseCell = getRandomCell(start, end);
                while (field[chooseCell] != null) {
                    chooseCell = getRandomCell(start, end);
                }

                field[chooseCell] = new PenaltyCell();
            }
        }
    }

    /**
     * Генерация клеток-магазинов.
     */
    public void generateShops() {
        for (int i = 0; i < size; i++) {
            if (field[i] == null) {
                field[i] = new Shop(i);
            }
        }
    }

    /**
     * Получение номера рандомной клетки конкретной линии поля.
     *
     * @param startOfLine Индекс клетки-начала линии поля.
     * @param endOfLine   Индекс клетки-конца линии поля.
     * @return Рандомная клетка линии поля.
     */
    public static int getRandomCell(int startOfLine, int endOfLine) {
        return RND.nextInt(endOfLine - startOfLine + 1) + startOfLine;
    }

    /**
     * Вывод поля.
     */
    public void print() {
        System.out.println("Here is the start field. Cells are numbered from 0 clockwise starting at " +
                "the top left. " + System.lineSeparator() + "That is, the top left cell is numbered 0, " +
                "the bottom right cell is " + lineEnd[1] + ". ");
        for (int line = 0; line < height; line++) {
            if (line == 0) {
                for (int i = 0; i < width; i++) {
                    System.out.print(field[i]);
                }
            } else if (line == height - 1) {
                for (int i = lineEnd[2]; i >= lineStart[2]; i--) {
                    System.out.print(field[i]);
                }
            } else {
                System.out.print(field[lineEnd[3] - line]);
                for (int i = 1; i < width - 1; i++) {
                    System.out.print("     ");
                }
                System.out.print(field[lineStart[1] + line]);
            }
            System.out.print(System.lineSeparator());
        }
    }

    /**
     * Вывод поля с точки зрения конкретного игрока.
     *
     * @param player Игрок.
     */
    public void print(Player player) {
        for (int line = 0; line < height; line++) {
            if (line == 0) {
                for (int i = 0; i < width; i++) {
                    if (field[i] instanceof Shop) {
                        setShopSymbol(field[i], player);
                    }
                    System.out.print(field[i]);
                }
            } else if (line == height - 1) {
                for (int i = lineEnd[2]; i >= lineStart[2]; i--) {
                    if (field[i] instanceof Shop) {
                        setShopSymbol(field[i], player);
                    }
                    System.out.print(field[i]);
                }
            } else {
                if (field[lineEnd[3] - line] instanceof Shop) {
                    setShopSymbol(field[lineEnd[3] - line], player);
                }
                System.out.print(field[lineEnd[3] - line]);
                for (int i = 1; i < width - 1; i++) {
                    System.out.print("     ");
                }
                if (field[lineStart[1] + line] instanceof Shop) {
                    setShopSymbol(field[lineStart[1] + line], player);
                }
                System.out.print(field[lineStart[1] + line]);
            }
            System.out.print(System.lineSeparator());
        }
    }

    /**
     * Задаёт символ клетки-магазина в зависимоти от игрока.
     *
     * @param cell   конкретная клетка-магазин, для которой требуется задать значение символа.
     * @param player Игрок, относительно которого задаётся символ.
     */
    private void setShopSymbol(Cell cell, Player player) {
        if (((Shop) cell).getOwner() == null) {
            cell.setSymbol("S");
            return;
        }

        if (((Shop) cell).getOwner().equals(player.name)) {
            cell.setSymbol("M");
            return;
        }

        cell.setSymbol("O");
    }
}
