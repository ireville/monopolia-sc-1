/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

public abstract class Cell {
    // Символ клетки (в зависимости от типа).
    protected String symbol = " ";

    /**
     * Задаёт символ клетки.
     *
     * @param newSymbol Новый символ клетки
     */
    public void setSymbol(String newSymbol) {
        symbol = newSymbol;
    }

    public void play(Player player) {
    }

    @Override
    public String toString() {
        return "| " + symbol + " |";
    }
}
