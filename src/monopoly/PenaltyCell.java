/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;

public class PenaltyCell extends Cell {
    private static final Random RND = new Random();
    public static final double penaltyCoeff = RND.nextDouble() * (0.1 - 0.01) + 0.01;

    public PenaltyCell() {
        this.symbol = "%";
    }

    /**
     * Реализует процесс игры на шрафной клетке.
     *
     * @param player Игрок
     */
    public void play(Player player) {
        long penalty = Math.round(player.getMoney() * penaltyCoeff);

        System.out.println((player.name.equals("You") ? "You are" : "Bot is") + " on the Penalty cell. " +
                (player.name.equals("You") ? "You are" : "Bot is") + " charged a fee: " + penalty + ". ");

        System.out.println("Now " + (player.name.equals("You") ? "your" : "Bot's") +
                " balance is: " + (player.getMoney() - penalty) + ". ");

        // Собираем с игрока штраф за попаданиена клетку.
        player.setMoney(player.getMoney() - penalty);
    }
}
