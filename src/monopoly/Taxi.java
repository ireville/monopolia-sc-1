/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;

public class Taxi extends Cell {
    private static final Random RND = new Random();
    private static int playFieldSize;

    public Taxi(int fieldSize) {
        this.symbol = "T";
        playFieldSize = fieldSize;
    }

    /**
     * Реализует процесс игры на клетке такси.
     *
     * @param player Игрок.
     */
    public void play(Player player) {
        int taxiDistance = RND.nextInt(5 - 3 + 1) + 3;

        System.out.printf((player.name.equals("You") ? "You are" : "Bot is") + " on the Taxi cell. " +
                        (player.name.equals("You") ? "You are" : "Bot is") + " shifted forward by %d cells. ",
                taxiDistance);

        player.setPosition(player.getPosition() + taxiDistance, playFieldSize);

        System.out.println("Now " + (player.name.equals("You") ? "you are" : "bot is") + " playing " +
                "on the cell number " + player.getPosition() + ". ");
    }
}
