/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

public class EmptyCell extends Cell {
    public EmptyCell() {
        this.symbol = "E";
    }

    /**
     * Реализует процесс игры на пустой клетке.
     */
    public void play(Player player) {
        System.out.println("Just relax there. ");
    }
}
