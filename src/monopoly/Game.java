/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private static final Random RND = new Random();
    private static int width, height, money;

    public static void main(String[] args) {
        if (!readInputData(args)) {
            System.out.println("Incorrect input data. ");
            return;
        }

        // Генерация и вывод поля.
        PlayField playField = new PlayField(height, width);
        playField.print();

        // Вывод коэффициентов.
        printCoeffs(playField);

        // Генерируем игроков и их очередность.
        Player player1 = RND.nextInt(2) == 1 ? new Player("You", money)
                : new Player("Bot", money);
        Player player2 = player1.name.equals("You") ? new Player("Bot", money)
                : new Player("You", money);
        printPlayersOrder(player1, player2);

        // Запускаем игру и выводим её результат.
        String result = game(playField, player1, player2);
        System.out.println(System.lineSeparator() + result);
    }


    /**
     * Пытается распарсить строку в целое число, лежащее в заданном промежутке.
     *
     * @param value Строка со входными данными.
     * @return Число, которое содержалось во входной строке.
     */
    public static int tryParse(String value, long lowerBound, long upperBound) {
        int num = Integer.parseInt(value);
        if (lowerBound <= num && num <= upperBound) {
            return num;
        } else {
            throw new NumberFormatException("Incorrect input data. ");
        }
    }

    /**
     * Проверяет корректность входных данных и преобразует их в нужный тип.
     *
     * @param args Входные данные.
     * @return Истина, если даннные корректны, ложь, если нет.
     */
    private static boolean readInputData(String[] args)     {
        if (args.length < 3) {
            return false;
        }

        try {
            height = tryParse(args[0], 6, 30);
            width = tryParse(args[1], 6, 30);
            money = tryParse(args[2], 500, 15000);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Реализует игру на клетке.
     *
     * @param playField Игровое поле.
     * @param player    Ирок, делающий ход
     * @param passive   Второй игрок.
     */
    public static void cellPlay(PlayField playField, Player player, Player passive) {
        Cell current = playField.field[player.getPosition()];

        if (current instanceof Taxi) {
            current.play(player);
            while (playField.field[player.getPosition()] instanceof Taxi) {
                current.play(player);
            }
            cellPlay(playField, player, passive);
        } else if (current instanceof Shop) {
            ((Shop) current).play(player, passive);
        } else {
            current.play(player);
        }
    }

    /**
     * Реализует ход игры.
     *
     * @param playField Игровое поле.
     * @param player    Игрок, делающий ход.
     * @param passive   второй игрок.
     */
    public static void makeMove(PlayField playField, Player player, Player passive) {
        System.out.println(System.lineSeparator() + "Move of the player: " + player.name + System.lineSeparator());

        // Генерируем шаг.
        int step = RND.nextInt(6) + 1 + RND.nextInt(6) + 1;
        System.out.println("You are going " + step + " cells forward from cell " + player.getPosition() + ". ");

        // Перемещаемся на клетку.
        player.setPosition(player.getPosition() + step, playField.size);

        cellPlay(playField, player, passive);

        printCurrentPlayersInfo(playField, player, passive);
    }

    /**
     * Реализует игру с последовательными ходами игроков.
     *
     * @param playField Игровое поле.
     * @param player1   Первый игрок.
     * @param player2   Второй игрок.
     * @return Результат игры.
     */
    public static String game(PlayField playField, Player player1, Player player2) {
        System.out.println("Press Enter to start the game...");
        (new Scanner(System.in)).nextLine();

        long i = 1;
        String looserName = "Unknown";
        try {
            while (player1.getMoney() >= 0 && player2.getMoney() >= 0) {
                if (i % 2 == 1) {
                    makeMove(playField, player1, player2);
                } else {
                    makeMove(playField, player2, player1);
                }

                i++;
                System.out.println("Press Enter for the next game step...");
                (new Scanner(System.in)).nextLine();
            }
        } catch (IllegalArgumentException e) {
            looserName = e.getMessage();
        } catch (Exception ex) {
            System.out.println("Unknown exception: " + ex.getMessage());
        }

        return looserName + (looserName.equals("You") ? " are" : " is") + " a bancrupt now. " +
                System.lineSeparator() + "GAME OVER. " + System.lineSeparator() + "The winner is: " +
                (looserName.equals("You") ? "Bot" : "You");
    }

    /**
     * Печатает в консоль исходные коэффициенты.
     */
    public static void printCoeffs(PlayField playField) {
        System.out.println(System.lineSeparator() + "Сoefficients: ");
        System.out.printf("    creditCoeff: %3.2f" + System.lineSeparator(), Bank.creditCoeff);
        System.out.printf("    debtCoeff: %3.2f" + System.lineSeparator(), Bank.debtCoeff);
        System.out.printf("    penaltyCoeff: %3.2f" + System.lineSeparator(), PenaltyCell.penaltyCoeff);

        System.out.println(System.lineSeparator() + "Shops information:");
        for (int i = 0; i < playField.size; i++) {
            if (playField.field[i] instanceof Shop) {
                System.out.println("    " + ((Shop) playField.field[i]).shopDescription());
            }
        }
        System.out.println("*all coefficients might have a deviation about +-0.01 from real value because " +
                "of the rounding before the output.");
    }

    /**
     * Печатает в консоль очередность игроков.
     *
     * @param player1 Первый игрок.
     * @param player2 Второй игрок.
     */
    public static void printPlayersOrder(Player player1, Player player2) {
        System.out.println(System.lineSeparator() + "Players: ");
        System.out.println("    The first player is: " + player1.name);
        System.out.println("    The second player is: " + player2.name);
        System.out.println("Balance of each player is: " + player2.getMoney() + System.lineSeparator());
    }

    /**
     * Печатает текущую информацию об игроке.
     *
     * @param player Игрок.
     * @param active Информация о том, его ли ход был завершен
     *               (нужно, так как о походившем выводится больше информации).
     */
    private static void printPlayerInfo(Player player, boolean active) {
        System.out.println("    " + player.name + ": ");
        System.out.println("        Field position: " + player.getPosition());
        System.out.println("        Balance: " + player.getMoney());
        if (active) {
            System.out.println("        Bank debt: " + player.getDebt());
            System.out.println("        All money spent: " + player.getAllMoneySpent());
        }
    }

    /**
     * Вывод информации после хода игрока.
     *
     * @param playField Игровое поле.
     * @param player    Игрок, ход которого завершился.
     * @param passive   Второй игрок.
     */
    private static void printCurrentPlayersInfo(PlayField playField, Player player, Player passive) {
        System.out.println(System.lineSeparator() + "Move ended. ");
        System.out.println("Current field: ");
        playField.print(player);

        System.out.println(System.lineSeparator() + "Information: ");
        printPlayerInfo(player, true);
        printPlayerInfo(passive, false);
    }
}
