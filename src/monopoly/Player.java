/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

public class Player {
    public String name;

    // Количество денег (баланс) игрока и размер задолженности банку.
    private long money;
    private long debt;

    // Количество всех денег, потраченных на покупку и улучшение магазина.
    private long allMoneySpent;

    // Текущая позиция на игровом поле.
    private int position;

    public Player(String name, long money) {
        this.name = name;
        this.money = money;
    }

    /**
     * Получение позиции игрока на игровом поле (номер клетки, на которой тот находится).
     *
     * @return Номер клетки.
     */
    public int getPosition() {
        return position;
    }

    /**
     * Задаёт позицию игрока на игровом поле (номер клетки, на которой тот находится).
     *
     * @param newPosition Новая позиция.
     * @param size        Размер игрового поля.
     */
    public void setPosition(int newPosition, int size) {
        position = newPosition % size;
    }

    /**
     * Получение денежного баланса игрока.
     *
     * @return Количество денег у игрока.
     */
    public long getMoney() {
        return money;
    }

    /**
     * Задаёт новый денежный баланс игрока (изменяет количество денег у него).
     *
     * @param newValue Новое значение баланса.
     */
    public void setMoney(long newValue) {
        if (newValue < 0) {
            money = newValue;
            throw new IllegalArgumentException(name);
        }
        money = newValue;
    }

    /**
     * Получение значение банковского долга у игрока.
     *
     * @return Величина долга.
     */
    public long getDebt() {
        return debt;
    }

    /**
     * Задаёт новое значение банковского долга у игрока.
     *
     * @param newValue Новое значение долга.
     */
    public void setDebt(long newValue) {
        debt = Math.max(newValue, 0);
    }

    /**
     * Получение размера всех средств, потраченных на покупку и улучшение магазинов.
     *
     * @return Количество потраченных денег.
     */
    public long getAllMoneySpent() {
        return allMoneySpent;
    }

    /**
     * Задаёт количество всех средств. потраченных на покупку и улучшение магазинов.
     *
     * @param newValue Новое значение потраченных денег.
     */
    public void setAllMoneySpent(long newValue) {
        allMoneySpent = newValue;
    }
}
