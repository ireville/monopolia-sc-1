/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;
import java.util.Scanner;

public class Shop extends Cell {
    private static final Random RND = new Random();
    private long N, K;
    private final double compensationCoeff, improvementCoeff;

    // Номер магазина на игровом поле.
    private final int id;

    // Владелец магазина.
    private String owner = null;

    public Shop(int id) {
        this.id = id;
        this.symbol = "S";

        N = RND.nextInt(500 - 50) + 50;
        K = Math.round(RND.nextDouble() * (0.9 * N - 0.5 * N) + 0.5 * N);
        compensationCoeff = RND.nextDouble() * (1 - 0.1) + 0.1;
        improvementCoeff = RND.nextDouble() * (2 - 0.1) + 0.1;
    }

    /**
     * Реализует процесс покупки магазина игроком.
     *
     * @param player Игрок
     */
    public void buy(Player player) {
        player.setMoney(player.getMoney() - N);
        owner = player.name;
        player.setAllMoneySpent(player.getAllMoneySpent() + N);
    }

    /**
     * Реализует процесс улучшения магазина игроком.
     *
     * @param player   Игрок.
     * @param newPrice Новая цена магазина.
     */
    public void upgrade(Player player, long newPrice) {
        N += Math.round(N * improvementCoeff);
        K += Math.round(K * compensationCoeff);
        player.setMoney(player.getMoney() - newPrice);
        player.setAllMoneySpent(player.getAllMoneySpent() + newPrice);
    }

    /**
     * Получение имени владельца магазина.
     *
     * @return Имя владельца.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Процесс игры на клетке магазина, у которого нет владельца.
     *
     * @param player Игрок
     */
    public void playWithNoOwner(Player player) {
        System.out.println((player.name.equals("You") ? "You are" : "Bot is") + " in the shop cell " + id + ". ");
        if (player.getMoney() >= N) {
            System.out.println("Would you like to buy shop for " + N + "$?");

            if (player.name.equals("You")) {
                if (getAnswer("Input Yes if you agree or No otherwise: ").equals("Yes")) {
                    buy(player);
                    System.out.println("You bought shop №" + id);
                } else {
                    System.out.println("You haven't bought the shop. ");
                }
            } else {
                if (RND.nextInt(2) == 1) {
                    System.out.println("Bot buys this shop. ");
                    buy(player);
                } else {
                    System.out.println("Bot doesn't buy this shop. ");
                }
            }
        } else {
            System.out.println("But " + player.name + " don't have enough (" + N + "$) money to buy it. ");
        }
    }

    /**
     * Процесс игры на клетке магазина, владельцем которого является текущий игрок.
     *
     * @param player Игрок.
     */
    public void playWithOwner(Player player) {
        System.out.println((player.name.equals("You") ? "You are in your" : "Bot is in his") + " shop " + id + ". ");
        long newPrice = Math.round(N * improvementCoeff);

        if (player.getMoney() >= newPrice) {
            System.out.println("Would you like to upgrade it for " + newPrice + "$?");

            if (player.name.equals("You")) {
                if (getAnswer("Input Yes if you agree or No otherwise: ").equals("Yes")) {
                    upgrade(player, newPrice);
                    System.out.println("You've upgraded the shop №" + id);
                } else {
                    System.out.println("You haven't upgraded the shop. ");
                }
            } else {
                if (RND.nextInt(2) == 1) {
                    System.out.println("Bot upgrades his shop. ");
                    upgrade(player, newPrice);
                } else {
                    System.out.println("Bot doesn't upgrade his shop. ");
                }
            }
        } else {
            System.out.println((player.name.equals("You") ? "You" : "Bot") + " don't have enough " +
                    "(" + newPrice + "$) money to upgrade it. ");
        }
    }

    /**
     * Процесс игры на клетке магазина.
     *
     * @param player Игрок.
     */
    public void play(Player player, Player passive) {
        if (owner == null) {
            playWithNoOwner(player);
        } else if (owner.equals(player.name)) {
            playWithOwner(player);
        } else {
            System.out.println((player.name.equals("You") ? "You are " : "Bot is ") + "in the shop cell "
                    + id + ". Shop owner: " + (player.name.equals("You") ? "Bot" : "You") +
                    ". Compensation " + K + " is charged.");
            System.out.println((player.name.equals("You") ? "Your" : "Bot's") + " balance now is: " +
                    (player.getMoney() - K));
            player.setMoney(player.getMoney() - K);
            passive.setMoney(passive.getMoney() + K);
        }
    }

    /**
     * Процесс получения ответа "Yes" или "No" от игрока, пока тот не ввдёт корректный.
     *
     * @param message Сообщение, выводимое пользователю.
     * @return Ответ пользователя.
     */
    public static String getAnswer(String message) {
        System.out.println(message);

        Scanner in = new Scanner(System.in);
        String input = in.next();
        while (!(input.equals("Yes") || input.equals("No"))) {
            System.out.println("Incorrect input. " + message);
            input = in.next();
        }

        return input;
    }

    /**
     * Вывод информации о магазине.
     *
     * @return Строкое представление информации о магазине.
     */
    public String shopDescription() {
        return String.format("Shop №%d: N = %d, K = %d, compensationCoefficient = %.2f, " +
                "improvementCoefficient = %.2g", id, N, K, compensationCoeff, improvementCoeff);
    }
}
