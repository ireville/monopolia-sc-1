/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package monopoly;

import java.util.Random;
import java.util.Scanner;

public class Bank extends Cell {
    private static final Random RND = new Random();

    public static final double creditCoeff = RND.nextDouble() * (0.2 - 0.002) + 0.002;
    public static final double debtCoeff = RND.nextDouble() * (3 - 1) + 1;

    public Bank() {
        this.symbol = "$";
    }

    /**
     * Реализует процесс получения игроком кредита.
     *
     * @param player  Игрок
     * @param desired Желаемый игроком кредит.
     */
    public static void getCredit(Player player, long desired) {
        // Даём кредит.
        player.setMoney(player.getMoney() + desired);

        // Записываем долг.
        player.setDebt(Math.round(desired * debtCoeff));
    }

    /**
     * Получает от игрока информацию о предоставлении кредита.
     *
     * @param message Сообщение, выводимое игроку.
     * @param player  Игрок.
     * @return Размер желаемого кредита.
     */
    public static long getCreditInfo(String message, Player player) {
        Scanner in = new Scanner(System.in);
        String input = getAnswer(message);

        if (input.equals("Yes")) {
            long given = Math.round(player.getAllMoneySpent() * creditCoeff);
            System.out.println("How many do you want to get? (the size of the credit must be a positive integer, " +
                    "not more than " + given + "$)");

            long desired = tryParse(in.next(), -1);
            while (desired <= 0 || desired > given) {
                System.out.println("Incorrect input. Credit must be a positive integer, not more than "
                        + given + "$: ");
                desired = tryParse(in.next(), -1);
            }

            return desired;
        }

        return 0;
    }

    /**
     * Требует от пользователя ответ "Yes" или "No", пока тот не введёт корректно.
     *
     * @param message Сообщение, выводимое пользователю.
     * @return Ответ пользователя.
     */
    public static String getAnswer(String message) {
        Scanner in = new Scanner(System.in);
        String input = in.next();
        while (!(input.equals("Yes") || input.equals("No"))) {
            System.out.println("Incorrect input. " + message);
            input = in.next();
        }

        return input;
    }

    /**
     * Пытается распарсить строку в целое число.
     *
     * @param value      Строка со входными данными.
     * @param defaultVal Значение по умолчанию, возвращаемое, если входную строку невозможно
     *                   синтерпретировать как целое число.
     * @return Число, которое содержалось во входной строке или значение по умолчанию.
     */
    public static long tryParse(String value, int defaultVal) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }

    /**
     * Реализует процесс сбора у игрока долга банку.
     *
     * @param player Игрок.
     */
    public static void collectDebt(Player player) {
        System.out.println("We collected your debt. Your current money balance: " +
                (player.getMoney() - player.getDebt()) + ". ");

        long debt = player.getDebt();
        player.setDebt(0);
        player.setMoney(player.getMoney() - debt);
    }

    /**
     * Осуществляет процесс игры на клетке банка.
     *
     * @param player Игрок.
     */
    public void play(Player player) {
        if (player.name.equals("Bot")) {
            System.out.println("Bot doesn't play in bank. ");
            return;
        }

        // Если игрок - должник банка.
        System.out.println("You are in the bank office. Your debt is: " + player.getDebt() + ". ");
        if (player.getDebt() != 0) {
            collectDebt(player);
            return;
        }

        // Если он ещё ничего не покупал и не улучшал.
        if (Math.round(player.getAllMoneySpent() * creditCoeff) == 0) {
            System.out.println("You can't get credit yet. ");
            return;
        }

        System.out.println("Would you like to get a credit? " + System.lineSeparator() + "Input Yes or No: ");
        long desired = getCreditInfo("Input Yes or No: ", player);
        if (desired == 0) {
            System.out.println("No credit given. ");
        } else {
            getCredit(player, desired);
            System.out.println(player.name + " were given a credit: " + desired);
            System.out.println("Your debt now is: " + player.getDebt());
        }
    }
}
